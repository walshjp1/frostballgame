﻿using UnityEngine;
using System.Collections;

public class RotateSpine_2 : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	private float maxY = 0.000534058f; //go - direction
	private float maxX = 16.303191f; //go + direction
	private float maxZ = 359.9982831f; //go - direction
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal2");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);
		float Y = steer * maxY;
		float X = steer * maxX;
		float Z = steer * maxZ;

		if (steer > 0) {
			transform.localRotation = Quaternion.Euler (7.899139f + X, -0.0001831055f - Y, 359.9982f);
		} else {
			transform.localRotation = Quaternion.Euler (7.899139f - X, -0.0001831055f - Y, 359.9982f);
		}
	}
}
