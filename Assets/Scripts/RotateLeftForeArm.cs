﻿using UnityEngine;
using System.Collections;

public class RotateLeftForeArm : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	private float maxY = 0f; //+ direction
	private float maxX = 0f; //- direction
	private float maxZ = 30.6947f; // + direction
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);
		float Y = steer * maxY;
		float X = steer * maxX;
		float Z = steer * maxZ;
		
		transform.localRotation = Quaternion.Euler(351.691f,10.04814f,281.3442f+Z);
	}
}
