﻿using UnityEngine;
using System.Collections;

public class VehicleJump2 : MonoBehaviour {
	public float JumpSpeed = 500000.0f;

	private bool firstJump;
	private bool isGroundedP;
	private bool isGroundedC;
	private int collisions;
	private int updateCount;
	private int actualCount;
	// Use this for initialization
	void Start () {
		firstJump = false;
		isGroundedP = false;
		isGroundedC = false;
		collisions = 0;
		updateCount = 0;
	}
	// Update is called once per frame
	void FixedUpdate () {
		if (updateCount == 0)
			updateCount = 1;
		else
			updateCount = 0;
		Debug.Log (gameObject.transform.position);
		Debug.Log (collisions.ToString());
		if (isGroundedC == true || isGroundedP == true)
			firstJump = false;
		if (Input.GetButtonDown ("Jump2") && !firstJump) {
			Jump ();
			firstJump = true;
		}
	}

	void OnCollisionEnter(Collision knownObject)
	{
		actualCount = updateCount;
		if (knownObject.gameObject.name == "Plane") {
			isGroundedP = true;
		} else if (knownObject.gameObject.name == "Cube_000") {
			isGroundedC = true;
		}
	}

	void OnCollisionExit(Collision knownObject)
	{
		if (knownObject.gameObject.name == "Plane") {
			isGroundedP = false;
		} else if (knownObject.gameObject.name == "Cube_000") {
			isGroundedC = false;
		}


	}



	void Jump() { 
		Rigidbody rb = GetComponent<Rigidbody>();
		//animation.Play ("jump_pose"); 
		rb.AddForce (Vector3.up * JumpSpeed);

	}
}
