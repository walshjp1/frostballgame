﻿using UnityEngine;
using System.Collections;

public class RotateRightForeArm2 : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	private float maxY = 0.6064f; //- direction
	private float maxX = 2.3247f; // - direction
	private float maxZ = 49.184f; // + direction
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal2");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);
		if (steer > 0) {
			float Y = steer * maxY;
			float X = steer * maxX;
			float Z = steer * maxZ;
		
			transform.localRotation = Quaternion.Euler (343.166f - X, 347.5438f - Y, 83.1601f + Z);
		} else {

			maxY = 0f; //+ direction
			maxX = 0f; //- direction
			maxZ = 30.6947f; // + direction

			float Y = -steer * maxY;
			float X = -steer * maxX;
			float Z = -steer * maxZ;
			
			//transform.localRotation = Quaternion.Euler (343.166f - X, 347.5438f + Y, 83.1601f -Z);
		}
	}
}
