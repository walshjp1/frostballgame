﻿using UnityEngine;
using System.Collections;

public class RotateNeck : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	private float maxY = 14.6974f; //+ direction
	private float maxX = 19.5605f; //- direction
	private float maxZ = 6.5702f; // - direction
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);
		float Y = steer * maxY;
		float X = steer * maxX;
		float Z = steer * maxZ;

		if (steer > 0) {
			transform.localRotation = Quaternion.Euler (341.1449f - X, Y, -Z);
		} else {
			transform.localRotation = Quaternion.Euler (341.1449f + X, Y, -Z);
		}
	}
}
