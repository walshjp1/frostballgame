﻿using UnityEngine;
using System.Collections;

public class RotateRightHand : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	private float maxY = 30.9308f; //+ direction
	private float maxX = 9.0925f; // - direction
	private float maxZ = 35.4446f; // - direction
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);
		float Y = steer * maxY;
		float X = steer * maxX;
		float Z = steer * maxZ;
		
		transform.localRotation = Quaternion.Euler(351.4221f - X,7.35527f + Y,349.6455f - Z);
	}
}
