﻿using UnityEngine;
using System.Collections;

public class RotateSpine22 : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	private float maxY = 18.43242f; //+ direction
	private float maxX = 0f; 
	private float maxZ = 0f; 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal2");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);
		float Y = steer * maxY;
		float X = steer * maxX;
		float Z = steer * maxZ;
		
		transform.localRotation = Quaternion.Euler(X,Y,Z);
	}
}
