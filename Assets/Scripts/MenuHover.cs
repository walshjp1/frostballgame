﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MenuHover : MonoBehaviour {

	public Sprite hoverSprite;
	public Sprite mainSprite;
	private Button button;

	// Use this for initialization
	void Start () {
		button = GetComponent<Button> ();
	}


	public void HoverImg(){
		button.image.overrideSprite = hoverSprite;
	}

	public void MainImg(){
		button.image.overrideSprite = mainSprite;
	}
	// Update is called once per frame
	void Update () {

	}
}
