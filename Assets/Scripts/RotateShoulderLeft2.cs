﻿using UnityEngine;
using System.Collections;

public class RotateShoulderLeft2 : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	private float maxY = 0f; //go - direction
	private float maxX = 0f; //go + direction
	private float maxZ = 22.8123f; //go + direction
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal2");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);
		float Y = steer * maxY;
		float X = steer * maxX;
		float Z = steer * maxZ;

		transform.localRotation = Quaternion.Euler(84.32025f + X,60.88144f + Y,136.108f + Z);
	}
}
