﻿using UnityEngine;
using System.Collections;

public class RotateRightShoulder : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	private float maxY = 8.9172f; //+ direction
	private float maxX = 38.46701f; //- direction
	private float maxZ = 0f; 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);

		if (steer > 0) {
			float Y = steer * maxY;
			float X = steer * maxX;
			float Z = steer * maxZ;
		
			transform.localRotation = Quaternion.Euler (79.71579f - X, 269.0913f + Y, 194.5811f);
		} else {
			maxY = 0f; //go - direction
			maxX = 0f; //go + direction
			maxZ = 22.8123f; //go + direction
			float Y = -steer * maxY;
			float X = -steer * maxX;
			float Z = -steer * maxZ;
			
			//transform.localRotation = Quaternion.Euler(79.71579f + X,269.0913f + Y,194.5811f + Z);
		}
	}
}
