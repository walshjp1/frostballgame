﻿using UnityEngine;
using System.Collections;

public class RotateRightSki2 : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal2");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);
		float steerAngle = steer * maxSteerAngle;

		transform.localRotation = Quaternion.Euler(0, steerAngle, 0);
	}
}
