﻿using UnityEngine;
using System.Collections;

public class RotateLeftArm2 : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	private float maxY = 6.519444f; //+ direction
	private float maxX = 26.6523254f; // - direction
	private float maxZ = 19.737765f; // - direction
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal2");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);
		float Y = steer * maxY;
		float X = steer * maxX;
		float Z = steer * maxZ;
		
		transform.localRotation = Quaternion.Euler(27.23013f - X,4.208496f + Y, 3.956665f - Z);
	}
}
