﻿using UnityEngine;
using System.Collections;

public class RotateSpine12 : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	private float maxY = 0f; 
	private float maxX = 0f; 
	private float maxZ = 11.4264f; // - directions
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal2");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);
		float Y = steer * maxY;
		float X = steer * maxX;
		float Z = steer * maxZ;

		if (steer > 0) {
			transform.localRotation = Quaternion.Euler (X, Y, -Z);
		} else {
			transform.localRotation = Quaternion.Euler (X, Y, -Z);
		}
	}
}
