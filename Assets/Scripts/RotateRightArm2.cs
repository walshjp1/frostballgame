﻿using UnityEngine;
using System.Collections;

public class RotateRightArm2 : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	private float maxY = 18.43242f; //+ direction
	private float maxX = 0f; 
	private float maxZ = 0f; 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal2");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);

		if (steer > 0) {
			float Y = steer * maxY;
			float X = steer * maxX;
			float Z = steer * maxZ;
		
			transform.localRotation = Quaternion.Euler (X, Y, Z);
		} else {
			maxY = 6.519444f; //+ direction
	        maxX = 26.6523254f; // - directio
	        maxZ = 19.737765f; // - direction

			float Y = -steer * maxY;
			float X = -steer * maxX;
			float Z = -steer * maxZ;
			
			//transform.localRotation = Quaternion.Euler (-X, -Y, -Z);




		}
	}
}
