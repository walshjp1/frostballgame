﻿using UnityEngine;
using System.Collections;

public class RotateLeftHand : MonoBehaviour {
	public float maxSteerAngle = 30.0f;
	public float steerSpeed = 0.5f;
	private float maxY = 20.635713f; //+ direction
	private float maxX = 2.5149997f; //- direction
	private float maxZ = 24.328314f; // - direction
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float rotateAmount = Input.GetAxis("Horizontal");
		float steer = steer = Mathf.Clamp(rotateAmount, -1.0f, 1.0f);
		float Y = steer * maxY;
		float X = steer * maxX;
		float Z = steer * maxZ;
		
		transform.localRotation = Quaternion.Euler(3.21608f - X,341.1873f + Y,3.21608f-Z);
	}
}
