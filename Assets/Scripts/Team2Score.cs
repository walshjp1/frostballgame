﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Team2Score : MonoBehaviour {
	public Text score2txt;
	public int score2;
	public GameObject ball;
	public GameObject p1;
	public GameObject p2;
	public GameObject spawnBall;
	public GameObject spawnP1;
	public GameObject spawnP2;
	// Use this for initialization
	void Start () {
		score2 = 0;
		score2txt.text = "Team 2: " + score2.ToString ();
	}
	
	// Update is called once per frame
	void Update () {
		GetComponent<MeshRenderer> ().enabled = false;
		score2txt.text = "Team 2: " + score2.ToString ();
		Debug.Log ("velocity" + p1.GetComponent<Rigidbody> ().velocity);
	}
	
	void OnTriggerEnter(Collider col){
		if (col.gameObject.tag == "Ball") {
			score2++;
			ball.gameObject.transform.position = spawnBall.gameObject.transform.position;
			ball.GetComponent<Rigidbody>().velocity = Vector3.zero;
			ball.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
			ball.GetComponent<Rigidbody>().Sleep();
			p1.gameObject.transform.position = spawnP1.gameObject.transform.position;
			p1.GetComponent<Rigidbody>().velocity = Vector3.zero;
			p1.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
			p1.gameObject.transform.rotation = spawnP1.gameObject.transform.rotation;
			p2.gameObject.transform.position = spawnP2.gameObject.transform.position;
			p2.GetComponent<Rigidbody>().velocity = Vector3.zero;
			p2.GetComponent<Rigidbody>().angularVelocity = Vector3.zero;
			p2.gameObject.transform.rotation = spawnP2.gameObject.transform.rotation;
		}
	}
}
